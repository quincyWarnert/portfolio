<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>
<body>
    <h2>Login</h2>
    <header>
    <nav>
        <ul>
            <li><a href="index.php">Home</a></li>
        </ul>
    </nav>
</header>
<form action="loginProcess.php" method="post">
   <div>
      <label for="email">Email</label>
      <input type="email" name="email">
  </div>
  <div>
    <label for="password">password</label>
    <input type="password" name="password">
  </div>       
      <input type="submit" value="Login" name="submit">
 </form>    
 <a href="register.php">Don't have an account?</a>
</body>
</html>

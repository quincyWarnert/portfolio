<?php



function makeConnection(){
  $servername ="localhost";
$username="root";
$password="";
try {
    $conn = new PDO("mysql:host=$servername;dbname=portfoliodb", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    return $conn;
  } catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
  }
}



function LoginUser($email,$password){
$conn=makeConnection();

try { 
 
  $stmt = $conn->prepare("SELECT * FROM user WHERE Email=:email AND Password=:password");
  $stmt->execute(array(":email"=>$email,":password"=>$password));
 
  
  $result= $stmt->fetch();  
   if(!$result)
   {
     return false;
   }
   elseif (password_verify($password,$result["Password"])) {
     session_start();

     $_SESSION['user_id']=$result["Id"];
     $_SESSION['name']=$result["Name"];
     
   }
} catch(PDOException $e) {
  echo "Error: " . $e->getMessage();
}
$conn = null;


}

?>